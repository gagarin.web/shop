enum Routes {
  HOME = "Home",
  SEARCH = "Search",
  FAVORITES = "Favorites",
  CART = "Cart",
  TABS = "Tabs",
  PRODUCT_DETAILS = "Product details",
}

export default Routes;
