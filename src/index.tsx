import React from "react";
import { Provider } from "react-redux";
import Toast from "react-native-toast-message";
import { NavigationContainer, NavigatorScreenParams } from "@react-navigation/native";
import { BottomTabBarProps, createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { SafeAreaView } from "react-native-safe-area-context";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Home, Search, Favorites, Cart, ProductDetails } from "src/screens";
import { Header, TabBar } from "src/components";
import { COLORS, ROUTES } from "src/constants";
import store from "src/store";

export type DetailsStackList = {
  [ROUTES.HOME]: undefined;
  [ROUTES.CART]: undefined;
  [ROUTES.PRODUCT_DETAILS]: {
    id: number;
  };
};

export type RootStackParamList = {
  [ROUTES.TABS]: NavigatorScreenParams<TabNavigatorParamList>;
  [ROUTES.PRODUCT_DETAILS]: NavigatorScreenParams<DetailsStackList>;
};

export type TabNavigatorParamList = {
  [ROUTES.HOME]: NavigatorScreenParams<undefined>;
  [ROUTES.SEARCH]: NavigatorScreenParams<undefined>;
  [ROUTES.FAVORITES]: NavigatorScreenParams<undefined>;
  [ROUTES.CART]: NavigatorScreenParams<undefined>;
};

const Stack = createNativeStackNavigator<RootStackParamList>();
const Tab = createBottomTabNavigator<TabNavigatorParamList>();

const Tabs = () => {
  return (
    <Tab.Navigator screenOptions={{ lazy: true, headerShown: false }} tabBar={(props: BottomTabBarProps) => <TabBar {...props} />}>
      <Tab.Screen name={ROUTES.HOME} component={Home} />
      <Tab.Screen name={ROUTES.SEARCH} component={Search} />
      <Tab.Screen name={ROUTES.FAVORITES} component={Favorites} />
      <Tab.Screen name={ROUTES.CART} component={Cart} />
    </Tab.Navigator>
  );
};

const RootNavigation = () => {
  return (
    <SafeAreaView edges={["top", "bottom"]} style={{ flex: 1, backgroundColor: COLORS.WHITE }}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Group screenOptions={{ gestureEnabled: false, header: Header }}>
            <Stack.Screen name={ROUTES.TABS} component={Tabs} />
          </Stack.Group>
          <Stack.Group screenOptions={{ gestureEnabled: false, headerShown: false  }}>
            <Stack.Screen name={ROUTES.PRODUCT_DETAILS} component={ProductDetails} />
          </Stack.Group>
        </Stack.Navigator>
      </NavigationContainer>
    </SafeAreaView>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <RootNavigation />
      <Toast />
    </Provider>
  );
};

export default App;
