import { all, fork } from "redux-saga/effects";
import listSagas from "src/store/list/sagas";
import cartSagas from "src/store/cart/sagas";

const rootSagas = function* () {
  yield all([
    fork(listSagas),
    fork(cartSagas),
  ]);
};

export default rootSagas;
