import { createStore, applyMiddleware, Store } from "redux";
import createSagaMiddleware from "redux-saga";
import { RootReducer } from "src/store/reducers";
import rootSagas from "src/store/sagas";
import { api, initAPI } from "src/store/api";

// ===========================================================================================
// MIDDLEWARE SETUP
// ===========================================================================================

const sagaMiddleware = createSagaMiddleware({ context: { api } });

const middlewares = [sagaMiddleware];

export type RootState = ReturnType<typeof RootReducer>;

export type Payload<T> = [{ payload: T }];

if (__DEV__) {
  const createDebugger = require("redux-flipper").default;
  middlewares.push(createDebugger());
}
// ===========================================================================================
// STORE CREATION
// ===========================================================================================

const store: Store = createStore(RootReducer, {}, applyMiddleware(...middlewares));

initAPI(store);

sagaMiddleware.run(rootSagas);

export default store;
