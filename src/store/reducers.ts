import { combineReducers } from "redux";

import ListReducer from "src/store/list";
import CartReducer from "src/store/cart";

export const RootReducer = combineReducers({
  list: ListReducer,
  cart: CartReducer,
});
