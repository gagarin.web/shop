import { CONSTANTS } from "src/constants";
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from "axios";
import type { Store } from "redux";
import { RootState } from "src/store";

export const api = axios.create({ baseURL: CONSTANTS.API_URL, timeout: CONSTANTS.TIME_REFRESH });

export const initAPI = (store: Store<RootState>) => {
  api.interceptors.request.use(async (config: AxiosRequestConfig) => {
    return config;
  });

  api.interceptors.response.use(
    (response: AxiosResponse) => {
      return response;
    },
    (error: AxiosError) => {
      return Promise.reject(error);
    },
  );
};
