import { all, fork, put, call, takeLeading } from "redux-saga/effects";
import Toast from 'react-native-toast-message';
import { getCart, setCart, addToCart, deleteCartItem, updateCartItem, deleteCart, addToCartLoading } from "src/store/cart";
import { AddCartProps, createApi, UpdateCartProps } from "src/api/cart";
import { api } from "src/store/api";

const cartApi = createApi(api);

export function* getCartSaga({ payload }: { payload: boolean }) {
  try {
    if (payload) {
      yield put(setCart.request(true));
    }
    yield put(setCart.failure(false));
    const { data } = yield call(() => cartApi.getCarts());
    yield put(setCart.success(data));
  } catch (e) {
    yield put(setCart.failure(true));
  } finally {
    yield put(setCart.request(false));
  }
}

export function* addToCartSaga({ payload }: { payload: AddCartProps }) {
  try {
    yield put(addToCartLoading(true));
    const { status } = yield call(() => cartApi.addToCart(payload));
    if (status === 200) {
      Toast.show({
        type: 'success',
        text1: 'Success',
        text2: `Product #${payload.id} added to cart`
      });
      yield put(getCart(false));
    }
  } catch (e) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: 'Something went wrong'
    });
  } finally {
    yield put(addToCartLoading(false));
  }
}

export function* deleteCartItemSaga({ payload }: { payload: number }) {
  try {
    yield call(() => cartApi.deleteCartItem(payload));
    yield put(getCart(false));
  } catch (e) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: 'Something went wrong'
    });
  }
}

export function* updateCartItemSaga({ payload }: { payload: UpdateCartProps }) {
  try {
    yield call(() => cartApi.updateCart(payload));
    yield put(getCart(false));
  } catch (e) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: 'Something went wrong'
    });
  }
}

export function* deleteCartSaga() {
  try {
    yield call(() => cartApi.deleteCart());
    yield put(getCart(false));
  } catch (e) {
    Toast.show({
      type: 'error',
      text1: 'Error',
      text2: 'Something went wrong'
    });
  }
}

function* watchCartSaga() {
  yield takeLeading(getCart, getCartSaga);
}

function* watchAddToCartSaga() {
  yield takeLeading(addToCart, addToCartSaga);
}

function* watchDeleteCartItemSaga() {
  yield takeLeading(deleteCartItem, deleteCartItemSaga);
}

function* watchUpdateCartItemSaga() {
  yield takeLeading(updateCartItem, updateCartItemSaga);
}

function* watchDeleteCartSaga() {
  yield takeLeading(deleteCart, deleteCartSaga);
}

export default function* root() {
  yield all([
    fork(watchCartSaga),
    fork(watchAddToCartSaga),
    fork(watchDeleteCartItemSaga),
    fork(watchUpdateCartItemSaga),
    fork(watchDeleteCartSaga),
  ]);
}
