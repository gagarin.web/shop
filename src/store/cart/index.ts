import { createReducer, createAction, createAsyncAction } from "typesafe-actions";
import produce from "immer";
import { Payload, RootState } from "src/store";
import { Action } from "redux";
import { AddCartProps, CartProps, UpdateCartProps } from "src/api/cart";

type STATE = {
  cart: Array<CartProps>;
  loading: boolean;
  error: boolean;
  addToCartLoading: boolean;
};

const INITIAL_STATE: STATE = {
  cart: [],
  loading: false,
  error: false,
  addToCartLoading: false,
};

export const selectCart = (state: RootState) => state.cart.cart;
export const selectCartLoading = (state: RootState) => state.cart.loading;
export const selectCartError = (state: RootState) => state.cart.error;
export const selectAddToCartLoading = (state: RootState) => state.cart.addToCartLoading;

export const selectCartProductsQuantity = (state: RootState) => state.cart.cart.length;

export const getCart = createAction("GET/CART", (withLoader: boolean) => withLoader)();

export const addToCart = createAction("ADD/CART", (params: AddCartProps) => params)();

export const addToCartLoading = createAction("ADD/CART_LOADING", (loading: boolean) => loading)();

export const deleteCart = createAction("DELETE/CART")();

export const deleteCartItem = createAction("DELETE/CART_ITEM", (itemId: number) => itemId)();

export const updateCartItem = createAction("UPDATE/CART_ITEM", (params: UpdateCartProps) => params)();

export const setCart = createAsyncAction(
  "SET/CART_LOADING",
  "SET/CART",
  "SET/CART_ERROR",
)<boolean, Array<any>, boolean>();

export default createReducer<STATE, Action>(INITIAL_STATE)
  .handleAction(
    setCart.request,
    produce<STATE, Payload<boolean>>((state, { payload }) => {
      state.loading = payload;
    }),
  )
  .handleAction(
    setCart.success,
    produce<STATE, Payload<Array<any>>>((state, { payload }) => {
      state.cart = payload;
    }),
  )
  .handleAction(
    setCart.failure,
    produce<STATE, Payload<boolean>>((state, { payload }) => {
      state.error = payload;
    }),
  )
  .handleAction(
    addToCartLoading,
    produce<STATE, Payload<boolean>>((state, { payload }) => {
      state.addToCartLoading = payload;
    }),
  );
