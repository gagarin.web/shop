import { all, fork, put, call, takeLeading } from "redux-saga/effects";
import { getList, setList } from "src/store/list";
import { createApi } from "src/api/items";
import { api } from "src/store/api";

const productApi = createApi(api);

export function* getListSaga({ payload }: { payload: boolean }) {
  try {
    if (payload) {
      yield put(setList.request(true));
    }
    yield put(setList.failure(false));
    const { data } = yield call(productApi.getItems);
    yield put(setList.success(data));
  } catch (e) {
    yield put(setList.failure(true));
  } finally {
    yield put(setList.request(false));
  }
}

function* watchListSaga() {
  yield takeLeading(getList, getListSaga);
}

export default function* root() {
  yield all([fork(watchListSaga)]);
}
