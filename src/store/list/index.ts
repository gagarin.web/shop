import { createReducer, createAction, createAsyncAction } from "typesafe-actions";
import produce from "immer";
import { Payload, RootState } from "src/store";
import { Action } from "redux";

type STATE = {
  list: any[];
  loading: boolean;
  error: boolean;
};

const INITIAL_STATE: STATE = {
  list: [],
  loading: false,
  error: false,
};

export const selectList = (state: RootState) => state.list.list;
export const selectListLoading = (state: RootState) => state.list.loading;
export const selectListError = (state: RootState) => state.list.error;

export const getList = createAction("GET/LIST", (withLoader) => withLoader) ();

export const setList = createAsyncAction(
  "SET/LIST_LOADING",
  "SET/LIST",
  "SET/LIST_ERROR",
)<boolean, Array<any>, boolean>();

export default createReducer<STATE, Action>(INITIAL_STATE)
  .handleAction(
    setList.request,
    produce<STATE, Payload<boolean>>((state, { payload }) => {
      state.loading = payload;
    }),
  )
  .handleAction(
    setList.success,
    produce<STATE, Payload<Array<any>>>((state, { payload }) => {
      state.list = payload;
    }),
  )
  .handleAction(
    setList.failure,
    produce<STATE, Payload<boolean>>((state, { payload }) => {
      state.error = payload;
    }),
  );
