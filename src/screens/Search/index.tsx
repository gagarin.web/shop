import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Gallery } from "src/components";
import { COLORS } from "src/constants";

const Search = () => {
  return (
    <View style={styles.view}>
      <Gallery />
      <Text>Search!</Text>
    </View>
  );
}

export default Search;

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,
  },
});
