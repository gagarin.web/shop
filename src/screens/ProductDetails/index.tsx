import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, Dimensions, ImageBackground, StyleSheet } from 'react-native';
import type { RouteProp } from '@react-navigation/native';
import { COLORS, ROUTES } from "src/constants";
import { useRoute } from "@react-navigation/native";
import { api } from "src/store/api";
import { SafeAreaView } from "react-native-safe-area-context";
import { DetailsStackList } from "src";
import { createApi, EmptyItemProps, ItemProps } from "src/api/items";
import { AxiosResponse } from "axios";
import { Loader, Button } from "src/components";
import BackIcon from "src/assets/left-arrow.svg";
import { useNavigation } from "@react-navigation/core";
import { useDispatch, useSelector } from "react-redux";
import { addToCart, selectAddToCartLoading } from "src/store/cart";

const productApi = createApi(api);
const { width } = Dimensions.get("window");

const ProductDetails = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const route: RouteProp<DetailsStackList, ROUTES.PRODUCT_DETAILS> = useRoute();
  const [item, setItem] = useState<ItemProps>(EmptyItemProps);
  const [loading, setLoading] = useState(false);
  const addToCartLoading = useSelector(selectAddToCartLoading);
  const handlePress = () => {
    dispatch(addToCart({ id: item.id, quantity: 1 }));
  };
  const getItem = async (id: number) => {
    try {
      setLoading(true);
      const response: AxiosResponse = await productApi.getItem(id);
      setItem(response.data);
    } catch (e) {
      navigation.goBack();
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    if (route?.params?.id) {
      getItem(route?.params?.id);
    }
  }, [route?.params?.id])

  if (loading) {
    return (
      <View style={styles.loader}>
        <Loader />
      </View>
    )
  }
  return (
    <SafeAreaView style={styles.wrapper}>
      <ImageBackground source={{ uri: item.image }} style={styles.image}>
        <TouchableOpacity activeOpacity={1} onPress={() => navigation.goBack()} style={styles.back}>
          <BackIcon width={36} height={36} />
        </TouchableOpacity>
      </ImageBackground>
      <View style={styles.titleView}><Text style={styles.titleText}>{item.name}</Text></View>
      <View><Text style={styles.descriptionText}>Bla bla text</Text></View>
      <View><Text style={styles.subDescriptionText}>Style #3655 OYKOG 1000</Text></View>
      <View><Text style={styles.priceText}>${item.price}</Text></View>
      <Button onPress={handlePress} title="Add to card" loading={addToCartLoading} />
    </SafeAreaView>
  );
}

export default ProductDetails;

const styles = StyleSheet.create({
  image: {
    width,
    height: width,
  },
  wrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  back: {
    justifyContent: "center",
    alignItems: "center",
    width: 36,
    height: 36,
    borderRadius: 36,
    backgroundColor: COLORS.WHITE,
    margin: 10,
  },
  titleView: {
    paddingTop: 10,
  },
  titleText: {
    fontSize: 16,
    fontWeight: "bold",
    color: COLORS.BLACK,
  },
  descriptionText: {
    fontSize: 16,
    fontWeight: "normal",
    lineHeight: 24,
    color: COLORS.BLACK,
  },
  subDescriptionText: {
    fontSize: 14,
    fontWeight: "normal",
    color: COLORS.DUSTY_GREY,
  },
  priceText: {
    fontSize: 18,
    fontWeight: "bold",
    color: COLORS.BLACK,
  },
});
