import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Gallery } from "src/components";
import { COLORS } from "src/constants";

const Favorites = () => {
  return (
    <View style={styles.view}>
      <Gallery />
      <Text>Favorites!</Text>
    </View>
  );
}

export default Favorites;

const styles = StyleSheet.create({
  view: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,
  },
});
