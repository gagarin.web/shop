import React, { useEffect } from 'react';
import { Dimensions, FlatList, RefreshControl, StyleSheet, Text, View } from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { getCart } from "src/store/cart";
import { Gallery, Loader, CartProduct, Button } from "src/components";
import { selectCart, selectCartLoading } from "src/store/cart";
import { COLORS, CONSTANTS } from "src/constants";

const { width } = Dimensions.get("window");

const Cart = () => {
  const dispatch = useDispatch();
  const cart = useSelector(selectCart);
  const loading = useSelector(selectCartLoading);
  const [refresh, setRefresh] = React.useState<boolean>(false);
  const handleRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      dispatch(getCart(false));
      setRefresh(false);
    }, CONSTANTS.REFRESH1000);
  };
  useEffect(() => {
    dispatch(getCart(true));
  }, []);
  if (loading) {
    return (
      <View style={styles.loader}>
        <Loader />
      </View>
    )
  }
  return (
    <View style={styles.wrapper}>
      {/*<Gallery />*/}
      <View style={styles.view}>
        <FlatList
          refreshControl={<RefreshControl refreshing={refresh} onRefresh={handleRefresh} tintColor={COLORS.BLACK} />}
          contentContainerStyle={styles.contentStyle}
          style={styles.flat}
          removeClippedSubviews={true}
          ListEmptyComponent={() => (
            <View style={styles.empty}><Text style={styles.nothingText}>Nothing found</Text></View>
          )}
          horizontal={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => (
            <View style={styles.separator} />
          )}
          ListFooterComponent={() => cart.length ? (
            <Button title="Proceed to buy" />
          ) : null}
          data={cart}
          renderItem={({ item }) => (
            <CartProduct product={item} />
          )}
        />
      </View>
    </View>
  );
}

export default Cart;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE,
  },
  view: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  contentStyle: {
    paddingBottom: 140,
  },
  flat: {
    width: width,
    backgroundColor: COLORS.WHITE,
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  separator: {
    borderBottomWidth: 1,
    borderBottomColor: COLORS.BLACK,
    width: width - 48,
    alignSelf: "center",
    height: 10,
    marginBottom: 10,
  },
  nothingText: {
    fontSize: 14,
    fontWeight: "normal",
    lineHeight: 24,
    color: COLORS.BLACK,
  },
});
