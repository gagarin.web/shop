import React, { useEffect } from 'react';
import { Dimensions, Text, View, FlatList, RefreshControl, StyleSheet } from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { Gallery, Loader, ListProduct } from "src/components";
import { getList, selectList, selectListLoading } from "src/store/list";
import { COLORS, CONSTANTS } from "src/constants";

const { width } = Dimensions.get("window");

const Home = () => {
  const dispatch = useDispatch();
  const list = useSelector(selectList);
  const loading = useSelector(selectListLoading);
  const [refresh, setRefresh] = React.useState<boolean>(false);
  const handleRefresh = () => {
    setRefresh(true);
    setTimeout(() => {
      dispatch(getList(false));
      setRefresh(false);
    }, CONSTANTS.REFRESH1000);
  };
  useEffect(() => {
    dispatch(getList(true));
  }, []);
  if (loading) {
    return (
      <View style={styles.loader}>
        <Loader />
      </View>
    )
  }
  return (
    <View style={styles.wrapper}>
      <Gallery />
      <View style={styles.view}>
        <FlatList
          contentContainerStyle={styles.contentStyle}
          columnWrapperStyle={styles.columnStyle}
          style={styles.flat}
          refreshControl={<RefreshControl refreshing={refresh} onRefresh={handleRefresh} tintColor={COLORS.BLACK} />}
          ListEmptyComponent={() => (
            <View style={styles.empty}><Text style={styles.nothingText}>Nothing found</Text></View>
          )}
          removeClippedSubviews={true}
          numColumns={2}
          horizontal={false}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => index.toString()}
          data={list}
          renderItem={({ item }) => (
            <ListProduct product={item} />
          )}
        />
      </View>
    </View>
  );
}

export default Home;

const styles = StyleSheet.create({
  loader: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: COLORS.WHITE
  },
  view: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  flat: {
    backgroundColor: COLORS.WHITE,
    width: width,
  },
  empty: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  columnStyle: {
    justifyContent: "space-between",
    paddingBottom: 0
  },
  contentStyle: {
    paddingBottom: 140,
  },
  nothingText: {
    fontSize: 14,
    fontWeight: "normal",
    lineHeight: 24,
    color: COLORS.BLACK,
  },
});
