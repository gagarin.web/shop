import React from "react";
import { ActivityIndicator } from "react-native";
import { COLORS } from "src/constants";

interface Props {
  color?: string;
  size?: number | "small" | "large" | undefined;
}

const Loader = ({ color = COLORS.STORM_GREY, size = "small" }: Props) => (
  <ActivityIndicator size={size} color={color} />
);

export default Loader;
