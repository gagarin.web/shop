import React, { FC } from "react";
import { Text, View, TouchableOpacity, ActivityIndicator, StyleSheet } from "react-native";
import { COLORS } from "src/constants";

export type ButtonProps = {
  title: string;
  loading?: boolean;
  onPress?(): void;
};

const Button: FC<ButtonProps> = ({ title, loading = false, onPress }) => {
  const opacity = loading ? 0.5 : 1;
  return (
    <TouchableOpacity disabled={loading} onPress={onPress} activeOpacity={1} style={[styles.wrapper, { opacity }]}>
      <View style={styles.view}>
        <Text style={styles.title}>{title}</Text>
        {loading ? <ActivityIndicator color={COLORS.WHITE} style={styles.indicator} /> : null}
      </View>
    </TouchableOpacity>
  );
};

export default Button;

const styles = StyleSheet.create({
  wrapper: {
    marginTop: 40,
    paddingHorizontal: 40,
    height: 40,
    backgroundColor: COLORS.BLACK,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
  view: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 16,
    color: COLORS.WHITE,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  indicator: {
    paddingLeft: 10,
  },
});
