import React, { useEffect, useState } from "react";
import { Text, View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/core";
import type { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { COLORS, ROUTES } from "src/constants";
import PlusIcon from "src/assets/plus.svg";
import MinusIcon from "src/assets/minus.svg";
import { CartProps } from "src/api/cart";
import { DetailsStackList } from "src/index";

const CartProduct = ({ product }: { product: CartProps }) => {
  const navigation = useNavigation<NativeStackNavigationProp<DetailsStackList, ROUTES.PRODUCT_DETAILS>>();
  const [quantity, setQuantity] = useState<number>(1);
  const navigateToDetails = () => {
    navigation.push(ROUTES.PRODUCT_DETAILS, { id: product.item_id });
  };
  const handleMinus = () => quantity > 1 && setQuantity(quantity - 1);
  const handlePlus = () => setQuantity(quantity + 1);
  useEffect(() => {
    setQuantity(product.quantity);
  }, [product])
  return (
    <TouchableOpacity activeOpacity={1} onPress={navigateToDetails} style={styles.wrapper}>
      <View style={styles.leftView}>
        <Image source={{ uri: product.image }} style={styles.image} />
        <View style={styles.leftSubView}>
          <TouchableOpacity onPress={handleMinus} activeOpacity={1} style={styles.button}>
            <MinusIcon />
          </TouchableOpacity>
          <View style={styles.quantity}><Text style={styles.quantityText}>{quantity}</Text></View>
          <TouchableOpacity onPress={handlePlus} activeOpacity={1} style={styles.button}>
            <PlusIcon />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.rightView}>
        <View>
          <Text style={styles.titleText}>{product.name}</Text>
          <View><Text style={styles.descriptionText}>Bla bls text</Text></View>
          <View><Text style={styles.subDescriptionText}>Style #3655 OYKOG 1000</Text></View>
        </View>
        <View><Text style={styles.priceText}>${product.price}</Text></View>
      </View>
    </TouchableOpacity>
  );
};

export default CartProduct;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: COLORS.WHITE,
    margin: 10,
    flexDirection: "row",
    justifyContent: "center",
  },
  leftView: {
    flex: 2,
    padding: 0,
    justifyContent: "center",
    alignItems: "center",
  },
  rightView: {
    flex: 3,
    justifyContent: "space-between",
    paddingVertical: 10,
    alignItems: "flex-start",
  },
  image: {
    width: 120,
    aspectRatio: 11 / 14,
  },
  leftSubView: {
    flexDirection: "row",
    justifyContent: "center",
    marginTop: 10,
  },
  priceText: {
    fontSize: 18,
    fontWeight: "bold",
    color: COLORS.BLACK,
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    color: COLORS.BLACK,
  },
  descriptionText: {
    fontSize: 16,
    fontWeight: "normal",
    lineHeight: 24,
    color: COLORS.BLACK,
  },
  subDescriptionText: {
    fontSize: 14,
    fontWeight: "normal",
    color: COLORS.DUSTY_GREY,
  },
  button: {
    width: 40,
    height: 30,
    backgroundColor: COLORS.BLACK,
    justifyContent: "center",
    alignItems: "center",
  },
  quantity: {
    width: 40,
    height: 30,
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderTopColor: COLORS.BLACK,
    borderBottomColor: COLORS.BLACK,
    justifyContent: "center",
    alignItems: "center",
  },
  quantityText: {
    color: COLORS.BLACK,
    fontSize: 14,
  },
});
