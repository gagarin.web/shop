import React, { useEffect } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import { BottomTabBarProps } from '@react-navigation/bottom-tabs/src/types'
import { SvgProps } from "react-native-svg";
import HeartIcon from "src/assets/heart.svg";
import HomeIcon from "src/assets/home.svg";
import SearchIcon from "src/assets/search.svg";
import CartIcon from "src/assets/basket.svg";
import { COLORS, ROUTES } from "src/constants";
import { useDispatch, useSelector } from "react-redux";
import { getCart, selectCartProductsQuantity } from "src/store/cart";

interface Tab {
  icon: React.FC<SvgProps>;
  badge?: number;
}

const TabBar = ({ state, navigation }: BottomTabBarProps) => {
  const dispatch = useDispatch();
  const cartProductsQuantity = useSelector(selectCartProductsQuantity);
  const tabsMap: { [key: string]: Tab } = {
    [ROUTES.HOME]: {
      icon: HomeIcon,
    },
    [ROUTES.SEARCH]: {
      icon: SearchIcon,
    },
    [ROUTES.FAVORITES]: {
      icon: HeartIcon,
    },
    [ROUTES.CART]: {
      icon: CartIcon,
      badge: cartProductsQuantity,
    },
  };
  useEffect(() => {
    dispatch(getCart(false));
  }, []);
  return (
    <View style={styles.wrapper}>
      {state.routes.map((route, index) => {
        const isFocused = state.index === index;
        const { icon: Icon, badge } = tabsMap[route.name];
        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });
          if (!isFocused && !event.defaultPrevented) {
            //@ts-ignore
            navigation.navigate({ name: route.name, merge: true });
          }
        };
        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };
        return (
          <TouchableOpacity
            key={route.name}
            onPress={onPress}
            onLongPress={onLongPress}
          >
            <Icon width={24} height={24} fill={isFocused ? COLORS.VIOLET : COLORS.BLACK} />
            {badge ? (
              <View style={styles.badge}>
                <View style={styles.view}>
                  <Text style={styles.text}>{badge}</Text>
                </View>
              </View>
            ) : null}
          </TouchableOpacity>
        );
      })}
    </View>
  );
};

export default TabBar;

const styles = StyleSheet.create({
  wrapper: {
    shadowRadius: 5,
    shadowColor: COLORS.BLACK,
    shadowOpacity: 0.1,
    shadowOffset: { width: 3, height: 3 },
    paddingVertical: 15,
    paddingHorizontal: 30,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "white",
    position: "absolute",
    bottom: 40,
    left: 10,
    right: 10,
    borderRadius: 40,
  },
  badge: {
    justifyContent: "center",
    alignItems: "center",
    position: "absolute",
    right: -10,
    top: -10,
    width: 18,
    height: 18,
    borderRadius: 18,
    backgroundColor: COLORS.WHITE,
  },
  view: {
    justifyContent: "center",
    alignItems: "center",
    width: 16,
    height: 16,
    borderRadius: 16,
    backgroundColor: COLORS.BLACK,
  },
  text: {
    fontWeight: "bold",
    fontSize: 10,
    color: COLORS.WHITE,
  },
});
