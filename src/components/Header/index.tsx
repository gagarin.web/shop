import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { SafeAreaView } from 'react-native-safe-area-context';
import MenuIcon from "src/assets/menu.svg";
import UserIcon from "src/assets/user.svg";
import { COLORS } from "src/constants";

const Header = () => {
  return (
    <SafeAreaView edges={["top"]} style={styles.wrapper}>
      <View style={styles.view}>
        <View style={styles.subView}>
          <MenuIcon width={34} height={34} fill={COLORS.BLACK} />
          <Text style={styles.title}>bagzz</Text>
        </View>
        <UserIcon width={39} height={39} fill={COLORS.BLACK} />
      </View>
    </SafeAreaView>
  );
};

export default Header;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: COLORS.WHITE,
  },
  view: {
    paddingHorizontal: 10,
    height: 56,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: 'center',
  },
  subView: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: 'center',
  },
  title: {
    paddingLeft: 10,
    fontSize: 16,
    color: COLORS.BLACK,
  },
});
