import React from "react";
import { Text, View, Dimensions, TouchableOpacity, Image, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/core";
import type { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { COLORS, ROUTES } from "src/constants";
import { DetailsStackList } from "src/index";
import { ItemProps } from "src/api/items";

const { width } = Dimensions.get("window");

const CARD_SEPARATOR_WIDTH = 10;

const ListProduct = ({ product }: { product: ItemProps }) => {
  const navigation  = useNavigation<NativeStackNavigationProp<DetailsStackList, ROUTES.PRODUCT_DETAILS>>();
  const navigateToDetails = () => {
    navigation.push(ROUTES.PRODUCT_DETAILS, { id: product.id });
  };
  return (
    <TouchableOpacity activeOpacity={1} onPress={navigateToDetails} style={styles.wrapper}>
      <View style={styles.view}>
        <Image source={{ uri: product.image }} style={styles.image} />
        <View style={styles.nameView}><Text style={styles.nameText}>{product.name}</Text></View>
        <View style={styles.shopView}><Text style={styles.shopText}>Shop now</Text></View>
      </View>
    </TouchableOpacity>
  );
};

export default ListProduct;

const styles = StyleSheet.create({
  wrapper: {
    width: (width - CARD_SEPARATOR_WIDTH) / 2,
    backgroundColor: COLORS.LIGHT_GREY,
    marginTop: 10
  },
  view: {
    padding: 10,
    alignItems: "center",
  },
  image: {
    flex: 1,
    width: width / 4,
    height: width / 3,
    marginHorizontal: 20,
    aspectRatio: 11 / 14,
  },
  nameView: {
    paddingTop: 10,
  },
  nameText: {
    fontSize: 14,
    fontWeight: "bold",
    color: COLORS.BLACK,
  },
  shopView: {
    paddingTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: COLORS.BLACK,
  },
  shopText: {
    fontSize: 14,
    fontWeight: "500",
    textTransform: "uppercase",
    color: COLORS.BLACK,
  },
});
