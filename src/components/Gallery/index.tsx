import React, { ReactNode, useRef, useState } from "react";
import {
  Dimensions,
  View,
  ScrollView,
  NativeSyntheticEvent,
  NativeScrollEvent,
  TouchableOpacity,
  Image,
  StyleSheet
} from "react-native";
import LeftIcon from "src/assets/left-arrow.svg";
import RightIcon from "src/assets/right-arrow.svg";
import { COLORS } from "src/constants";

const { width } = Dimensions.get("window");

export const galleryItemUrl = `https://picsum.photos/${width}?grayscale`;

type GalleryProps = {
  children?: ReactNode;
};

const Gallery = ({ children }: GalleryProps): JSX.Element => {
  const scrollRef = useRef<ScrollView | null>(null);
  const [currentScreenIndex, setCurrentScreenIndex] = useState<number>(0);
  const handleOnScroll = (e: NativeSyntheticEvent<NativeScrollEvent>) => {
    const {
      nativeEvent: {
        contentOffset: { x },
      },
    } = e;
    setCurrentScreenIndex(Math.round(Number(x.toFixed()) / Number(width.toFixed())));
  };
  const handleOnLeft = () => {
    scrollRef?.current?.scrollTo({ x: (currentScreenIndex - 1) * width, y: 0, animated: true });
  };
  const handleOnRight = () => {
    scrollRef?.current?.scrollTo({ x: (currentScreenIndex + 1) * width, y: 0, animated: true })
  };
  return (
    <View style={styles.wrapper}>
      <ScrollView
        ref={scrollRef}
        scrollEventThrottle={5}
        horizontal
        pagingEnabled
        showsHorizontalScrollIndicator={false}
        onScroll={handleOnScroll}>
        {[galleryItemUrl, galleryItemUrl, galleryItemUrl].map((item, index) => (
          <View key={item + index} style={styles.imageView}>
            <Image
              source={{
                uri: item,
              }}
              resizeMode="cover"
              style={styles.image}
            />
          </View>
        ))}
        {children}
      </ScrollView>
      <View style={styles.navigation}>
        <TouchableOpacity activeOpacity={1} onPress={handleOnLeft} style={styles.navigationButton}>
          <LeftIcon width={34} height={34} fill={COLORS.WHITE} />
        </TouchableOpacity>
        <TouchableOpacity activeOpacity={1} onPress={handleOnRight} style={styles.navigationButton}>
          <RightIcon width={34} height={34} fill={COLORS.WHITE} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Gallery;

const styles = StyleSheet.create({
  wrapper: {
    width: width,
    height: width * 0.6,
    marginBottom: 10,
  },
  imageView: {
    flex: 1,
    alignItems: "center",
    position: "relative",
    flexDirection: "row",
  },
  image: {
    width: width,
    height: width * 0.6,
  },
  navigation: {
    position: "absolute",
    bottom: -10,
    right: 10,
    flexDirection: "row",
  },
  navigationButton: {
    width: 50,
    height: 50,
    backgroundColor: COLORS.BLACK,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: 5,
  },
});
