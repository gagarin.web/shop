import { AxiosInstance, AxiosResponse } from "axios";
import { CONSTANTS } from "src/constants";

const cartUrl = `${CONSTANTS.API_SUB_URL}/cart`;

export type CartProps = {
  item_id: number;
  price: number;
  name: string;
  image: string;
  quantity: number;
}

export type AddCartProps = {
  id: number;
  quantity: number;
}

export type UpdateCartProps = {
  itemId: number;
  quantity: number;
}

interface Api {
  getCarts: () => Promise<AxiosResponse<Array<CartProps>>>;
  addToCart: (params: AddCartProps) => Promise<AxiosResponse>;
  deleteCart: () => Promise<AxiosResponse>;
  deleteCartItem: (itemId: number) => Promise<AxiosResponse>;
  updateCart: (params: UpdateCartProps) => Promise<AxiosResponse>;
}

export const createApi = (httpClient: AxiosInstance): Api => {
  const { get, post, delete: del, put } = httpClient;
  return {
    getCarts: () => {
      return get(cartUrl);
    },
    addToCart: (params) => {
      return post(`${cartUrl}`, { params });
    },
    deleteCart: () => {
      return del(`${cartUrl}`);
    },
    deleteCartItem: (itemId: number) => {
      return del(`${cartUrl}/${itemId}`);
    },
    updateCart: ({ itemId, quantity }) => {
      return put(`${cartUrl}/${itemId}`, { quantity });
    },
  };
};
