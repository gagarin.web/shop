import { AxiosInstance, AxiosResponse } from "axios";
import { CONSTANTS } from "src/constants";
import { galleryItemUrl } from "src/components/Gallery";

const itemsUrl = `${CONSTANTS.API_SUB_URL}/items`;

export const EmptyItemProps = {
  id: 0,
  price: 0,
  name: "",
  image: galleryItemUrl,
  description: "",
}

export type ItemProps = {
  id: number;
  price: number;
  name: string;
  image: string;
  description?: string;
}

interface Api {
  getItems: () => Promise<AxiosResponse<Array<ItemProps>>>;
  getItem: (id: number) => Promise<AxiosResponse<ItemProps>>;
}

export const createApi = (httpClient: AxiosInstance): Api => {
  const { get } = httpClient;
  return {
    getItems: () => {
      return get(itemsUrl);
    },
    getItem: (id: number) => {
      return get(`${itemsUrl}/${id}`);
    },
  };
};
